# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Trekkie::Application.config.secret_key_base = 'b408dfb53927e529b313a626b31d08bab310a69eb0746104e1510a8ef414fd8d5a5f975e287cd8062db5504040332771633682eb016dc25566ad6890e5700707'
