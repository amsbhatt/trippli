class DropJoinTable < ActiveRecord::Migration
  def change
    drop_join_table :origins, :visits
  end
end
