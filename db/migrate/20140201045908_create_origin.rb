class CreateOrigin < ActiveRecord::Migration
  def change
    create_table :origins do |t|
    	t.string :name
    	t.string :code
    	t.timestamps
    end
  end
end
