class CreateNotices < ActiveRecord::Migration
  def change
    create_table :notices do |t|
      t.datetime :published_date
      t.string :code
      t.text :link
      t.string :category
      t.timestamps
    end
  end
end
