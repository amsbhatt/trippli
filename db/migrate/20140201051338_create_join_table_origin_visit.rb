class CreateJoinTableOriginVisit < ActiveRecord::Migration
  def change
    create_join_table :origins, :visits do |t|
      t.index [:origin_id, :visit_id]
      t.index [:visit_id, :origin_id]
      t.timestamps
    end
  end
end
