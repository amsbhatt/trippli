class CreateConsulates < ActiveRecord::Migration
  def change
    create_table :consulates do |t|
      t.string :name
      t.text :address
      t.string :email
      t.string :phone_number
      t.string :fax_number
      t.string :emergency_number
      t.integer :visit_id
      t.timestamps
    end
  end
end
