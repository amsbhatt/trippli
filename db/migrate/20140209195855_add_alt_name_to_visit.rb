class AddAltNameToVisit < ActiveRecord::Migration
  def change
    add_column :visits, :alt_name, :string
    add_column :details, :last_updated, :date
  end
end
