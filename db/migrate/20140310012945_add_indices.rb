class AddIndices < ActiveRecord::Migration
  def change
    add_index :notices, :visit_id
    add_index :consulates, :visit_id
  end
end
