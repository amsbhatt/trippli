class CreateDetails < ActiveRecord::Migration
  def change
    create_table :details do |t|
      t.hstore :data
      t.integer :origin_id
      t.integer :visit_id
      t.timestamps
    end
    add_index :details, :origin_id
    add_index :details, :visit_id
  end
end
