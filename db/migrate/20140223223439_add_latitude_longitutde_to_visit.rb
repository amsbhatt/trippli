class AddLatitudeLongitutdeToVisit < ActiveRecord::Migration
  def change
    add_column :visits, :longitude, :string
    add_column :visits, :latitude, :string
  end
end
