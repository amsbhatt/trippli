class CreateSites < ActiveRecord::Migration
  def change
    create_table :sites do |t|
      t.string :name
      t.integer :state_id
      t.timestamps
    end
    add_index :sites, :state_id
  end
end
