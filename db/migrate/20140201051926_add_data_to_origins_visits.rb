class AddDataToOriginsVisits < ActiveRecord::Migration
  def change
    add_column :origins_visits, :data, :hstore
  end
end
