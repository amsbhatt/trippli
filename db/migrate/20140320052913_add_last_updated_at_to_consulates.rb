class AddLastUpdatedAtToConsulates < ActiveRecord::Migration
  def change
    add_column :consulates, :last_updated, :date
  end
end
