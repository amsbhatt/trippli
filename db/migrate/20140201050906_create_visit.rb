class CreateVisit < ActiveRecord::Migration
  def change
    create_table :visits do |t|
    	t.string :name
    	t.string :code
    	t.timestamps
    end
  end
end
