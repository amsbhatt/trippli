# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140320052913) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "hstore"

  create_table "consulates", force: true do |t|
    t.string   "name"
    t.text     "address"
    t.string   "email"
    t.string   "phone_number"
    t.string   "fax_number"
    t.string   "emergency_number"
    t.integer  "visit_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.date     "last_updated"
  end

  add_index "consulates", ["visit_id"], name: "index_consulates_on_visit_id", using: :btree

  create_table "details", force: true do |t|
    t.hstore   "data"
    t.integer  "origin_id"
    t.integer  "visit_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.date     "last_updated"
  end

  add_index "details", ["origin_id"], name: "index_details_on_origin_id", using: :btree
  add_index "details", ["visit_id"], name: "index_details_on_visit_id", using: :btree

  create_table "notices", force: true do |t|
    t.datetime "published_date"
    t.string   "code"
    t.text     "link"
    t.string   "category"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "visit_id"
    t.string   "title"
  end

  add_index "notices", ["visit_id"], name: "index_notices_on_visit_id", using: :btree

  create_table "origins", force: true do |t|
    t.string   "name"
    t.string   "code"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sites", force: true do |t|
    t.string   "name"
    t.integer  "state_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "sites", ["state_id"], name: "index_sites_on_state_id", using: :btree

  create_table "states", force: true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "visits", force: true do |t|
    t.string   "name"
    t.string   "code"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "alt_name"
    t.string   "longitude"
    t.string   "latitude"
    t.hstore   "data"
  end

end
