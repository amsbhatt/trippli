require 'spec_helper'

describe Visit do
  it { should have_many(:origins) }
  it { should have_many(:details) }
  it { should have_many(:notices) }
  it { should have_one(:consulate) }
  it { validates_presence_of(:name) }
end