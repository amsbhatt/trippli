require 'spec_helper'

describe State do 
	let(:state) {State.create(:name => "California")}
	
	describe "validations" do
		it { should validate_presence_of(:name)}
		it { should have_many(:sites)}
	end

	describe "one state to many sites relationship" do
		let!(:site1) {Site.create(:name => "Lake Tahoe", :state_id => state.id)}
		let!(:site2) {Site.create(:name => "Alcatraz", :state_id => state.id)}
	
		it "should have many sites" do
			expect(state.sites.count).to eq 2
		end
	end

	describe "State::STATES" do
		it "validates inclusion of states in State::STATES" do
			State::STATES.each do |value|
				state.name = value
				state.should be_valid
			end
			state.name = 'WhaddyaThink'
			state.should_not be_valid
		end

		it { expect(State::STATES.count).to eq 60 }
	end
end