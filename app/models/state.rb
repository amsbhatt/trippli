class State < ActiveRecord::Base
  STATES = Country["US"].states.each_value.map {|state| state["name"]}

  validates :name, :presence => true, :inclusion => {:in => State::STATES}
  has_many :sites
end