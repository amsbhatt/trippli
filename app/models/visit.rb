class Visit < ActiveRecord::Base
  has_many :origins
  has_many :details
  has_many :notices
  has_one :consulate
  
  store_accessor :data, [ 
    :currency_code, 
    :currency_name, 
    :currency_symbol, 
    :current_rate 
  ]

  validates :name, presence: true 

  after_update :flush_cache
  after_commit :flush_cache

  IGNORE_COUNTRIES = [
    "Bouvet Island",
    "British Indian Ocean Territory",
    "Christmas Island",
    "Cocos (Keeling) Islands",
    "Cook Islands", 
    "Faroe Islands",
    "Falkland Islands (Malvinas)",
    "French Southern Territories",
    "Heard and McDonald Islands", 
    "Isle of Man",
    "Netherlands Antilles",
    "Norfolk Island",
    "Palestinian Territory, Occupied",
    "Saint Helena",
    "Saint Pierre And Miquelon",
    "South Georgia and the South Sandwich Islands",
    "Svalbard And Jan Mayen",
    "United States Minor Outlying Islands",
    "Virgin Islands, U.S.",
    "Wallis and Futuna",
    "Åland Islands",
    "United States",
    "Western Sahara",
    "Guernsey",
    "Jersey",
    "Niue",
    "Pitcairn",
    "Réunion",
    "Tokelau",
    "Mayotte"
  ]

  UPDATE_COUNTRIES = {
    "Brunei Darussalam" => "Brunei",
    "Holy See (Vatican City State)" => "Holy See",
    "Iran, Islamic Republic Of" => "Iran",
    "Côte D'Ivoire" => "Côte d'Ivoire",
    "Lao People's Democratic Republic" => "Laos",
    "Macedonia, the Former Yugoslav Republic Of" => "Macedonia",
    "Micronesia, Federated States Of" => "Micronesia",
    "Moldova, Republic of" => "Moldova",
    "Syrian Arab Republic" => "Syria",
    "Taiwan, Republic Of China" => "Taiwan",
    "Tanzania, United Republic of" => "Tanzania",
    "Venezuela, Bolivarian Republic of" => "Venezuela",
    "Virgin Islands, British" => "British Virgin Islands",
    "Cape Verde" => "Cabo Verde",
    "Russian Federation" => "Russia",
    "Macao" => "Macau",
    "Korea, Democratic People's Republic Of" => "Korea (North)",
    "Korea, Republic of" => "Korea (South)",
    "Saint Vincent And The Grenedines" => "Saint Vincent and the Grenedines",
    "Congo" => "Congo, Republic of",
    "Congo, The Democratic Republic Of The" => "Congo, Democratic Republic of the",
    "Saint Kitts And Nevis" => "Saint Kitts and Nevis"
  }

  SET_ALT_NAMES = {
    "Saint Barthélemy" => "FrenchWestIndies",
    "Bonaire, Sint Eustatius and Saba" => "bonaire-st-eustatius-saba",
    "Congo, Democratic Republic of the" => "congo-kinshasa",
    "Côte d'Ivoire" => "cote-divoire",
    "Hong Kong" => "hongkong",
    "Saint Kitts and Nevis" => "st-kitts-nevis",
    "Korea (North)" => "korea-north",
    "Korea (South)" => "korea-south",
    "Saint Lucia" => "st-lucia",
    "Saint Martin" => "FrenchWestIndies",
    "Papua New Guinea" => "papau-new-guinea",
    "Turks and Caicos Islands" => "turks-and-caicos-islands",
    "Saint Vincent and the Grenedines" => "st-vincent-the-grenadines",
    "Micronesia" => "federated-states-of-micronesia",
    "Andorra" => "spain-and-andorra",
    "Congo, Republic of" => "congo-brazzaville",
    "Curaçao" => "curacao",
    "Spain" => "spain-and-andorra",
    "Greenland" => "denmark",
    "Gambia" => "the-gambia",
    "Guadeloupe" => "FrenchWestIndies",
    "Bahamas" => "the-bahamas",
    "Switzerland" => "switzerland-and-liechtenstein",
    "Gibraltar" => "united-kingdom",
    "Myanmar" => "burma",
    "Martinique" => "FrenchWestIndies",
    "Portugal" => "Portugal",
    "Sao Tome and Principe" => "sao-tome-principe"
  }

  SET_COORDINATES = {
    "Curaçao" => {"longitude" => "69 00 W", "latitude" => "12 18 N"},
    "Bonaire, Sint Eustatius and Saba" => {"longitude" => "68 25 W", "latitude" => "12 18 N"},
    "British Virgin Islands" => {"longitude" => "64 62 W", "latitude" => "18 42 N"},
    "Guadeloupe" => {"longitude" => "61 58 W", "latitude" => "16 25 N"},
    "Martinique" => {"longitude" => "61 00 W", "latitude" => "14 66 N"},
    "Sint Maarten" => {"longitude" => "63 05 W", "latitude" => "18 06 N"}
  }

  SET_CURRENCY = {
    "Saint Barthélemy" => {"currency_code" => "EUR", "currency_name" => "Euro"}, 
    "Macau" => {"currency_code" => "MOP", "currency_name" => "Macanese pataca"}, 
    "Madagascar" => {"currency_code" => "MGA", "currency_name" => "Malagasy ariary"}, 
    "South Sudan" => {"currency_code" => "SSP", "currency_name" => "South Sudanese pound"}, 
    "Turkmenistan" => {"currency_code" => "TMT", "currency_name" => "Turkmenistan manat"}, 
    "Congo, Democratic Republic of the" => {"currency_code" => "CDF", "currency_name" => "Congolese franc"}, 
    "Congo, Republic of" => {"currency_code" => "XAF", "currency_name" => "Central African CFA franc"}, 
    "Equatorial Guinea" => {"currency_code" => "XAF", "currency_name" => "Central African CFA franc"}, 
    "Macedonia" => {"currency_code" => "MKD", "currency_name" => "Macedonian denar"},
    "Vanuatu" => {"currency_code" => "VUV", "currency_name" => "Vanuatu vatu"}
  }

  def self.update_names
    UPDATE_COUNTRIES.each do |key, value|
      if self.find_by_name(key)
        self.find_by_name(key).update_attributes(name: value)
      else
        puts "could not find country #{key}"
      end
    end
  end

  def self.set_altnames
    SET_ALT_NAMES.each do |key, value|
      if self.find_by_name(key)
        self.find_by_name(key).update_attributes(alt_name: value)
      else
        puts "could not find country #{key}"
      end
    end
  end

  def self.set_coordinates
    SET_COORDINATES.each do |key, value|
      if self.find_by_name(key)
        self.find_by_name(key).update_attributes(latitude: value["latitude"], longitude: value["longitude"])
      else
        puts "could not update coordinates #{key}"
      end
    end
  end

  def self.set_currency
    SET_CURRENCY.each do |key, value|
      if self.find_by_name(key)
        self.find_by_name(key).update_attributes(currency_code: value["currency_code"], currency_name: value["currency_name"])
      else
        puts "could not set currency for #{key}"
      end
    end
  end

  def self.check_name(warning)
    self.all.collect(&:name).any? { |x| return @name = x if warning =~ /#{x}/ }
  end

  def set_currency
    begin
      unless self.currency_code && self.currency_name && self.currency_symbol
        self.update_attributes(currency_code: Country[self.code].currency.code, currency_name: Country[self.code].currency.name, currency_symbol: Country[self.code].currency.symbol)
      end
    rescue Exception => e
      puts "Currency not found for visit #{self.id}"
    end
  end

  #Caching
  def self.cached_visits
    # Rails.cache.fetch([self, 'all_visits'], expires_in: 1.day) { self.all.collect{ |v| [v.name, v.name]} }
    Rails.cache.fetch("AllVisits/#{Date.today}") do 
      collect = self.all.collect{ |v| [v.name, v.name]}
      Rails.cache.write("AllVisits/#{Date.today}", collect)
      self.cached_visits
    end
  end

  def self.cached_find(name)
    # Rails.cache.fetch([name]) { find_by_name(name) }
    Rails.cache.fetch("AllVisits/#{name}/#{Date.today}") do 
      visit = find_by_name(name)
      Rails.cache.write("AllVisits/#{name}/#{Date.today}", visit)
      self.cached_find(name)
    end
  end

  def cached_details
    # Rails.cache.fetch([self, 'visit_details'], expires_in: 1.day) { details.first }
    Rails.cache.fetch("Visit/#{id}/#{updated_at.to_i}") do 
      visit = details.first
      Rails.cache.write("Visit/#{id}/#{updated_at.to_i}", visit)
      self.cached_details
    end
  end

  # def cached_notice
  #   Rails.cache.fetch([self, 'visit_notice'], expires_in: 1.day) { notices.first } if self.notices.first
  # end

  def cached_consulate
    # Rails.cache.fetch([self, 'visit_consulate'], expires_in: 1.day) { consulate.slice(:name, :address, :email, :phone_number, :fax_number, :emergency_number) }
    Rails.cache.fetch("Visit/#{id}/Consulate/#{updated_at.to_i}") do 
      visit = consulate.slice(:name, :address, :email, :phone_number, :fax_number, :emergency_number)
      Rails.cache.write("Visit/#{id}/Consulate/#{updated_at.to_i}", visit)
      self.cached_consulate
    end
  end

  def cached_currency
    # Rails.cache.fetch([self, 'visit_currency'], expires_in: 1.day) { Country[self.code].currency }
    Rails.cache.fetch("Visit/#{id}/Currency/#{updated_at.to_i}") do 
      visit = Country[self.code].currency
      Rails.cache.write("Visit/#{id}/Currency/#{updated_at.to_i}", visit)
      self.cached_currency
    end
  end  

  def flush_cache
    # Rails.cache.delete([self.class.name])
    Rails.cache.clear
  end

  #Coordinates Conversion - Google Maps
  def convert_latitude
    conversion(self.latitude)
  end

  def convert_longitude
    conversion(self.longitude)
  end

  def conversion(value)
    cord = value.split(" ")
    direction = cord[2]
    cord.pop[2]
    format = cord.join(".").to_f
    (direction == "W" || direction == "S") ? -format : format
  end

  #Current currency rate

  #OPENEXCHANGE RATES
  # def self.open_exchange_rates
  #   # response = HTTParty.get("http://openexchangerates.org/api/latest.json?app_id=99aadd1dc7d34fe792bbfffdad2f6aa7", format: :json)
  #   # return response.parsed_response["rates"]
  #   Rails.cache.fetch("exchange_rates", :expires_in => 1.day) do
  #     begin
  #       response = HTTParty.get("http://openexchangerates.org/api/latest.json?app_id=99aadd1dc7d34fe792bbfffdad2f6aa7", format: :json)
  #       return response.parsed_response["rates"]
  #     rescue
  #       false
  #     end
  #   end
  # end

  def current_exchange_rate
    begin
      if self.current_rate.present?
        self.current_rate
      else
        dollars = 1.to_money(:USD)
        dollars.exchange_to(self.currency_code.to_sym).to_s
      end
    rescue Exception => e
      return nil
    end
  end
end