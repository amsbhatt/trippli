class Consulate < ActiveRecord::Base
  belongs_to :visit, touch: true

  def self.details_to_array(opts)
    if opts["details"].include?("\r\n")
      d_to_a = opts["details"].split("\r\n")
      self.set_details(opts.merge({"details" => d_to_a}))
    elsif opts["details"].include?("\n")
      self.set_details(opts.merge({"details" => opts["details"].split("\n")}))
    else
      puts "Details not split #{opts["details"]}"
      return false
    end
  end

  def self.set_details(opts)
    details = opts["details"]
    new_consulate = {}

    i = 0
    details.each_with_index do |detail, index|
      if i == index
        if detail.include?("Emergency")
          new_consulate["emergency"] = self.text(detail, "Emergency Telephone")
        elsif detail.include?("Fax")
          new_consulate["fax"] = self.text(detail, "Fax")
        elsif detail.include?("Email")
          mail = self.text(detail, "Email")
          mail[0] = '' if mail[0].blank?
          new_consulate["email"] = mail.gsub(" ", "")
        elsif detail.include?("Embassy")
          new_consulate["name"] = detail.strip
        elsif detail.include?("Telephone")
          new_consulate["phone"] = self.text(detail, "Telephone")
        end
        i += 1
      else
        puts "error occured with consulate parsing for visit #{opts["visit_id"]}"
      end
    end
    self.create_consulate(new_consulate.merge({"visit_id" => opts["visit_id"], "address" => opts["address"], "last_updated" => opts["last_updated"].gsub("\n", "").strip.split(":")[1].strip}))
  end

  def self.create_consulate(details)
    self.create(
      name: details["name"], 
      address: details["address"], 
      email: details["email"], 
      emergency_number: details["emergency"], 
      fax_number: details["fax"], 
      phone_number: details["phone"],
      visit_id: details["visit_id"],
      last_updated: details["last_updated"]
    )
    puts "Consulate created #{details["name"]} for visit #{details["visit_id"]}"
  end

  def self.text(detail, param)
    if detail.split(param).length > 1
      detail.split(param)[1].strip
    else
      ""
    end
  end

  def self.parse_page(page, visit_id)
    self.details_to_array({
      "last_updated" => page.search(".updated_date .datecomp").text,
      "details" => page.search('.contact_list').text,
      "address" => page.search('.emphasize').text,
      "visit_id" => visit_id
      })
  end

  def date_convert
    unless self.last_updated.nil?
      update_date = self.last_updated
      update_date.strftime("%B #{update_date.day}, %Y")
    end
  end
end