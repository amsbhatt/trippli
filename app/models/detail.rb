class Detail < ActiveRecord::Base
  belongs_to :origin
  belongs_to :visit, touch: true

  SET_DETAILS = {
    "Northern Mariana Islands" => {
      "requirements" => "U.S. Citizens and Lawful Permanent Residents (LPR's) who travel directly between parts of the United States, which includes Guam, Puerto Rico, U.S. Virgin Islands, American Samoa, Swains Island and the Commonwealth of the Northern Mariana Islands (CNMI), without touching at a foreign port or place, are not required to present a valid U.S. Passport or U.S. Green Card.  However, it is recommended that travelers bring a government issued photo ID and copy of birth certificate.", 
      "vaccinations" => "Make sure you are up-to-date on routine vaccines before every trip. These vaccines include measles-mumps-rubella (MMR) vaccine, diphtheria-tetanus-pertussis vaccine, varicella (chickenpox) vaccine, polio vaccine, and your yearly flu shot.", 
      "passport validity" => "Not required if traveled directly from United States", 
      "blank passport pages" => "Not required", 
      "tourist visa required" => "No", 
      "currency restrictions for exit" => "None", 
      "currency restrictions for entry" => "None"
      },
    "American Samoa" => {
      "requirements" => "All visitors to American Samoa require a passport valid for six months or more, a return ticket or onward ticket and enough funds to support your stay", 
      "vaccinations" => "Make sure you are up-to-date on routine vaccines before every trip. These vaccines include measles-mumps-rubella (MMR) vaccine, diphtheria-tetanus-pertussis vaccine, varicella (chickenpox) vaccine, polio vaccine, and your yearly flu shot.", 
      "passport validity" => "6 months", 
      "blank passport pages" => "One page required for entry stamp", 
      "tourist visa required" => "No", 
      "currency restrictions for exit" => "None", 
      "currency restrictions for entry" => "None"
      },
    "Guam" => {
      "requirements" => "U.S. Citizens and Lawful Permanent Residents (LPR's) who travel directly between parts of the United States, which includes Guam, Puerto Rico, U.S. Virgin Islands, American Samoa, Swains Island and the Commonwealth of the Northern Mariana Islands (CNMI), without touching at a foreign port or place, are not required to present a valid U.S. Passport or U.S. Green Card.  However, it is recommended that travelers bring a government issued photo ID and copy of birth certificate.", 
      "vaccinations" => "Make sure you are up-to-date on routine vaccines before every trip. These vaccines include measles-mumps-rubella (MMR) vaccine, diphtheria-tetanus-pertussis vaccine, varicella (chickenpox) vaccine, polio vaccine, and your yearly flu shot.", 
      "passport validity" => "Not required if traveled directly from United States", 
      "blank passport pages" => "Not required", 
      "tourist visa required" => "No", 
      "currency restrictions for exit" => "None", 
      "currency restrictions for entry" => "None"
      },
    "Puerto Rico" => {
      "requirements" => "U.S. Citizens and Lawful Permanent Residents (LPR's) who travel directly between parts of the United States, which includes Guam, Puerto Rico, U.S. Virgin Islands, American Samoa, Swains Island and the Commonwealth of the Northern Mariana Islands (CNMI), without touching at a foreign port or place, are not required to present a valid U.S. Passport or U.S. Green Card.  However, it is recommended that travelers bring a government issued photo ID and copy of birth certificate.", 
      "vaccinations" => "Make sure you are up-to-date on routine vaccines before every trip. These vaccines include measles-mumps-rubella (MMR) vaccine, diphtheria-tetanus-pertussis vaccine, varicella (chickenpox) vaccine, polio vaccine, and your yearly flu shot.", 
      "passport validity" => "Not required if traveled directly from United States", 
      "blank passport pages" => "Not required", 
      "tourist visa required" => "No", 
      "currency restrictions for exit" => "None", 
      "currency restrictions for entry" => "None"
    }
  }

  #Rake task methods
  def self.set_details
    SET_DETAILS.each do |key, value|
      visit = Visit.find_by_name(key)
      unless (visit && visit.details.empty?)
        Detail.create(:data => value, :origin_id => Origin.find_by_name("United States").id, :visit_id => Visit.find_by_name(key).id, :last_updated => Date.today)
      end
    end
  end

  def self.parse_page(page, visit_id)
    last_updated_at = self.parse_update(page.search(".updated_date .datecomp").text)
    details = self.details(page.search('#quick_facts_section .text').to_html)
    puts "#{page.title} - ##{visit_id}"
    if page.search('.expandos dt:nth-child(5)').text.include?("Entry")
      @requirements = page.search('.expandos dd:nth-child(6)').text.strip
    end
    self.create_details(visit_id, details.merge("requirements" => @requirements), last_updated_at)
  end

  def self.create_details(country_id, details, updated_at)
    begin
      self.create(
        origin_id: Origin.find_by_code("US").id, 
        visit_id: country_id, 
        last_updated: updated_at,
        data: details
        )
    rescue Exception => e
      puts "Could not save: #{country}, Error #{e}"
    end
  end

  def self.parse_update(date_string)
    date_text = date_string.gsub("\n","").strip.split(": ")
    date_text[1]
  end

  def self.details(details)
    split_new_line = details.gsub("\n ", "").split("\n")
    details_array = split_new_line.select{|x| x if x.include?("h3")}
    detail_object = {}
    details_array.map{|detail| detail_object[detail.match(/<h3>(.+?):<\/h3>/)[1].downcase] = (detail.match(/<p>(.+?)<\/p>/) ? detail.match(/<p>(.+?)<\/p>/)[1].gsub("<br>","") : nil) }
    detail_object
  end

  def date_convert
    update_date = self.last_updated
    update_date.strftime("%B #{update_date.day}, %Y")
  end
end