class Origin < ActiveRecord::Base
  has_many :visits
  has_many :details

  def self.cached_origins
  	# Expire on Oct 19th, 2015
  	Rails.cache.fetch("AllOrigins/1445268970") do 
  		collect = self.all.collect{ |o| [o.name, o.id]}
  		Rails.cache.write("AllOrigins/1445268970", collect)
  		self.cached_origins
  	end
  end
end