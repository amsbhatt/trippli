module ApplicationHelper
  def format_detail(value)
    if (value.blank? || value.empty? || value == "N/A")
      "No Information"
    else
      value[0] = '' if value[0].blank?
      value.capitalize
    end
  end

  def format_title(key)
    key.split(" ").map(&:capitalize).join(" ")
  end

  def format_date(date)
    date.in_time_zone("Pacific Time (US & Canada)").strftime("%b %d, %Y")
  end

  def format_date_time(date)
    date.in_time_zone("Pacific Time (US & Canada)").strftime("%b %d, %Y %I:%M%p")
  end

  def disclaimer
    "All data is sourced from <a href='http://travel.state.gov/content/travel/english.html' target='_blank'>Travel State Gov</a> and checked daily for updates. It is the user's responsibility to check for accuracy.".html_safe
  end

  def currency_format(visit)
    if visit.current_exchange_rate.nil?
      "<div class='key clearfix'>Currency</div><br><div class='value'>#{visit.currency_code} - #{visit.currency_name}</div>".html_safe
    else
      "<div class='key'>Exchange Rate</div><div class='subtitle clearfix'>(#{format_date_time(DateTime.now)} PST)</div><div class='value'>$1 USD = #{visit.currency_symbol}#{visit.current_exchange_rate} #{visit.currency_code} (#{visit.currency_name})<br><a href='#{currency_disclaimer_path}' target='_blank'>currency disclaimer</a></div>".html_safe
    end
  end

  def is_mobile?
    request.user_agent =~ /Mobile|webOS/
  end
end