// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require_tree .

$(function () {
  $("#visit").change(function(){
    $("#visit").closest("form").submit();
    var country = $("#visit").find(":selected").text();
    document.title = "US to " + country + " - trippli"
    window.location.hash = country;
  })
});

$(document).ready(function() {
  if (window.location.hash) {
    var country = unescape(window.location.hash).replace("#", "").replace("/", "");
    $("#visit").find('option').filter(function(){return this.text == country}).prop('selected', true);
    document.title = "US to " + country + " - trippli"
    $("#visit").closest("form").submit();
  }
});