class SitemapController < ApplicationController
  layout nil

  def index
    headers['Content-Type'] = 'application/xml'

    last_visit = Visit.last
    if stale?(:etag => last_visit, :last_modified => last_visit.updated_at.utc)
      respond_to do |format|
        format.xml { @visits = Visit.all }
      end
    end
  end
end