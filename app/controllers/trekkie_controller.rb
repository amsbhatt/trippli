class TrekkieController < ApplicationController
  def show
    @origin = Origin.cached_origins
    @visits = Visit.order("name ASC").cached_visits

    if params[:visit]
      @visit = Visit.cached_find(params[:visit])
      @notice = @visit.notices.first
      find_detail = @visit.cached_details
      @requirements = find_detail.data.slice("requirements").values[0]
      @details = find_detail.data.delete_if{|k,v| k == "requirements"} unless find_detail.nil?
      @consulate = @visit.cached_consulate if @visit.consulate
      expires_in 1.day
      if stale?(etag: @visit, last_modified: @visit.updated_at, public: true)
        respond_to do |format|
          format.html
          format.js
        end
      end
    end
  end

  def currency_disclaimer
  end
end
