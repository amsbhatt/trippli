desc "Get country specific data"
task :facts => :environment do
  agent = Mechanize.new
  Visit.all.each do |visit|
    unless visit.alt_name.nil?
      begin
        page = agent.get("http://travel.state.gov/content/passports/english/country/#{visit.alt_name}.html")
        unless visit.details.present? && (visit.details.first.date_convert == Detail.parse_update(page.search(".updated_date .datecomp").text))
          visit.details.first.destroy if visit.details.first.present?
          Detail.parse_page(page, visit.id)
        end
      rescue Exception => e
        puts "Country not found #{visit.name} : ID ##{visit.id}, Error found #{e}"
      end
    end
  end
  Detail.set_details
end


task :consulates => :environment do
  agent = Mechanize.new
  Visit.all.each do |visit|
    unless visit.alt_name.nil?
      begin
        page = agent.get("http://travel.state.gov/content/passports/english/country/#{visit.alt_name}.html")
        unless visit.consulate.present? && (visit.consulate.date_convert == Detail.parse_update(page.search(".updated_date .datecomp").text))
          visit.consulate.destroy if visit.consulate.present?
          Consulate.parse_page(page, visit.id)
        end
      rescue Exception => e
        puts "Country not found #{visit.name} : ID ##{visit.id}, Error found #{e}"
      end
    end
  end
end