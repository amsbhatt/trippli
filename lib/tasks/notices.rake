desc "This task is called to pull the travel notices (warnings)"
task :warn => :environment do
  puts "in Warnings"
  warnings = HTTParty.get("http://travel.state.gov/_res/rss/TWs.xml")
  warnings["rss"]["channel"]["item"].each do |warning|
    title = warning["title"]
    check_warning = Notice.find_by_title(title)
    country = Visit.check_name(title)
    
    if (check_warning && check_warning.published_date != warning["pubDate"] && country) || (country)
      check_warning.destroy if check_warning
      visit = Visit.find_by_name(country)
      Notice.create(visit_id: visit.id, title: title, published_date: warning["pubDate"], link: warning["link"], category: "warning", code: visit.code)
      puts "Warning created #{Notice.last.id}: #{Notice.last.code}"
    end
  end
end