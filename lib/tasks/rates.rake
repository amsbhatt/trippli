desc "set_current_rate"
task :exchange_rate => :environment do
  response = HTTParty.get("http://openexchangerates.org/api/latest.json?app_id=99aadd1dc7d34fe792bbfffdad2f6aa7", format: :json)
  rates = response.parsed_response["rates"]

  rates.each do |key, value|
    begin
      visit = Visit.where("(data -> 'currency_code')='#{key}'").first
      visit.current_rate = value
      visit.save
      puts "Visit Current rate #{visit.name}, #{visit.current_rate}"
    rescue Exception => e
      puts "Visit could not be found #{key}"
    end
  end
end