desc "setup data"
task :import => :environment do
  Origin.create(name: "United States", code: "US") unless Origin.find_by_code("US")
  Country.all.each do |country|
    unless Visit::IGNORE_COUNTRIES.include?(country[0])
      find_country = Country[country[1]]
      Visit.create(name: country[0], code: country[1], latitude: find_country.data["latitude"], longitude: find_country.data["longitude"]) unless Visit.find_by_name(country[0])
    end
  end
  Visit.update_names
  Visit.set_altnames
  Visit.set_coordinates
end

desc "update_data"
task :alt => :environment do
  Visit.all.each do |v|
    if v.alt_name.nil?
      if (v.name =~ /\sand\s/) || (v.name =~ /\sAnd\s/)
        v.update_attributes(alt_name: v.name.gsub(/\sand\s/, "-").downcase)
      elsif (v.name.include?(" "))
        v.update_attributes(alt_name: v.name.gsub(" ", "-").downcase)
      else
        v.update_attributes(alt_name: v.name.downcase)
      end
    end
  end
end

desc "set_currency_codes"
task :currency_codes => :environment do
  Visit.set_currency
  Visit.all.each do |v|
    v.set_currency
  end
end